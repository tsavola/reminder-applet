#!/usr/bin/env python
#
# Copyright (c) 2010 Timo Savola

import pygtk

pygtk.require("2.0")

import os

import gnomeapplet
import gtk
import pango

text_filename = os.path.join(os.environ["HOME"], ".reminder")
text_placeholder = "Nothing to remember"

def text_load():
	try:
		file = open(text_filename)
	except:
		file = None

	if file:
		text = " ".join([line.strip() for line in file if line])
		file.close()
	else:
		text = None

	return text

def text_save(text):
	file = open(text_filename, "w")
	print >>file, text
	file.close()

def applet_factory(applet, iid):
	box = gtk.EventBox()

	attrs = pango.AttrList()
	attrs.insert(pango.AttrWeight(pango.WEIGHT_BOLD, 0, -1))

	label = gtk.Label(text_load() or text_placeholder)
	label.set_attributes(attrs)

	window = gtk.Window(gtk.WINDOW_TOPLEVEL)
	window.set_title("Edit Reminder")
	window.set_resizable(False)

	entry = gtk.Entry()
	entry.set_width_chars(80)

	box.connect("button-release-event", box_button_release, label, window, entry)
	entry.connect("activate", entry_activate, window, label)
	window.connect("delete-event", window_delete)

	applet.add(box)
	box.add(label)

	window.add(entry)

	applet.show_all()

	return True

def box_button_release(box, event, label, window, entry):
	if event.button == 1:
		text = text_load()
		if text:
			entry.set_text(text)
		entry.set_position(-1)
		window.show_all()

	return False

def entry_activate(entry, window, label):
	text = entry.get_text()
	text_save(text)
	label.set_text(text or text_placeholder)
	window.hide()

def window_delete(window, event):
	window.hide()
	return True

gnomeapplet.bonobo_factory("OAFIID:GNOME_ReminderApplet_Factory", gnomeapplet.Applet.__gtype__, "reminder", "0", applet_factory)
