DESTDIR		:= 
PREFIX		:= /usr/local
SERVERDIR	:= $(PREFIX)/lib/bonobo/servers
BINDIR		:= $(PREFIX)/lib/reminder-applet

build:

install:
	mkdir -p $(DESTDIR)$(SERVERDIR) $(DESTDIR)$(BINDIR)
	sed "s,@BINDIR@,$(BINDIR),g" GNOME_ReminderApplet.server.in > $(DESTDIR)$(SERVERDIR)/GNOME_ReminderApplet.server
	cp reminder-applet.py $(DESTDIR)$(BINDIR)/

.PHONY: build install
